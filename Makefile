SRCS = $(wildcard *.cpp)
OBJS-o0 = $(patsubst %.cpp,out/%-o0,$(SRCS))
OBJS-o1 = $(patsubst %.cpp,out/%-o1,$(SRCS))
OBJS-o2 = $(patsubst %.cpp,out/%-o2,$(SRCS))
OBJS-o3 = $(patsubst %.cpp,out/%-o3,$(SRCS))
CC=clang++
.PHONY: O0 O1 O2 O3 clean

O0: $(OBJS-o0)
	echo "O0 Done"
O1: $(OBJS-o1)
	echo "O1 Done"
O2: $(OBJS-o2)
	echo "O2 Done"
O3: $(OBJS-o3)
	echo "O3 Done"

all: O0 O1 O2 O3

out/%-o0: %.cpp
	$(CC) -g -O0 $< -o $@
out/%-o1: %.cpp
	$(CC) -g -O1 $< -o $@
out/%-o2: %.cpp
	$(CC) -g -O2 $< -o $@
out/%-o3: %.cpp
	$(CC) -g -O3 $< -o $@
clean:
	rm out/*
