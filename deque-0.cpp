#include <iostream>
#include <deque>
 
int main()
{
    // Create a deque containing integers
    std::deque<int> d = {7, 5, 16, 8};
    std::deque<int> d1;
 
    // Add an integer to the beginning and end of the deque
    d.push_front(13);
    d.push_back(25);
    d1.push_back(22);
 
    // Iterate and print values of deque
    for(int n : d) {
        std::cout << n << '\n';
    }
}
