#include <functional>
#include <iostream>
int main(int argc, char *argv[])
{
	std::function<int(int)> lamb = [] (int val) -> int { return val * 100; };
	std::cout << lamb(1) << std::endl;
	return 0;
}
