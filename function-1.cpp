#include <iostream>  
#include <vector>  
#include <list>  
#include <map>  
#include <set>  
#include <string>  
#include <algorithm>  
#include <functional>  
#include <memory>  
using namespace std;  
  
typedef std::function<int(int)> Functional;  
  
int TestFunc(int a)  
{  
    return a;  
}  
  
auto lambda = [](int a)->int{return a;};  
  
class Functor  
{  
public:  
    int v0;
    int v1;
    int v2;
    int v3;
    int v4;
    Functor(int a0, int a1, int a2, int a3, int a4):v0(a0), v1(a1), v2(a2), v3(a3), v4(a4){};
    int test()
    {
	    return v2 + v3 + v4;
    }
    int operator() (int a)  
    {  
        return a + v0 + test();  
    }  
};  
  
class CTest  
{  
public:  
    int Func(int a)  
    {  
        return a;  
    }  
    static int SFunc(int a)  
    {  
        return a;  
    }  
};  
  
int main(int argc, char* argv[])  
{  
    Functional obj = TestFunc;  
    int res = obj(0);  
    cout << "normal function : " << res << endl;  
  
    Functional obj1 = lambda;  
    res = obj1(1);  
    cout << "lambda expression : " << res << endl;  
  
    Functor functorObj(100, 101, 102, 103, 104);  
    Functional obj2 = functorObj;  
    res = obj2(2);  
    cout << "functor : " << res << endl;  
  
    CTest t;  
    Functional obj3 = std::bind(&CTest::Func, &t, std::placeholders::_1);  
    res = obj3(3);  
    cout << "member function : " << res << endl;  
  
    Functional obj4 = CTest::SFunc;  
    res = obj4(4);  
    cout << "static member function : " << res << endl;  
    return 0;  
}  
