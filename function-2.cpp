#include <iostream>  
#include <vector>  
#include <list>  
#include <map>  
#include <set>  
#include <string>  
#include <algorithm>  
#include <functional>  
#include <memory>  
using namespace std;  
  
typedef std::function<int(int, int)> Functional;  
typedef std::function<int(int, int, int)> Functional1;  
  
class CTest  
{  
public:  
    int mem_0;
    int mem_1;
    CTest(int m0, int m1):mem_0(m0), mem_1(m1){}
    int Func(int a)  
    {  
        return mem_0 + a;  
    }  
    int Func1(int a, int b)  
    {  
        return mem_0 + mem_1 + a + b;  
    }  
    static int SFunc(int a)  
    {  
        return a;  
    }  
};  
  
class CTest1
{  
public:  
    unsigned long long mem_0;
    unsigned long long mem_1;
    unsigned long long mem_2;
    CTest1(unsigned long long m0, unsigned long long m1):mem_0(m0), mem_1(m1), mem_2(m1){}
    int Func(int a)  
    {  
        return mem_0 + a;  
    }  
    int Func1(int a, int b, int c)  
    {  
        return mem_0 + mem_1 + mem_2 + a + b + c;  
    }  
    static int SFunc(int a)  
    {  
        return a;  
    }  
}; 
int main(int argc, char* argv[])  
{  
    int res = 0;  
    int res1 = 0;  
  
    CTest t(1, 2);  
    Functional obj3 = std::bind(&CTest::Func1, &t, std::placeholders::_1, std::placeholders::_2);  
    res = obj3(3, 4);  
    cout << "member function : " << res << endl;  

    CTest1 t1(1, 2);  
    Functional1 obj4 = std::bind(&CTest1::Func1, &t1, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);  
    res1 = obj4(3, 4, 5);  
    cout << "member function : " << res1 << endl;  
    return 0;  
}  
