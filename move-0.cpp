#include <iostream>
#include <string.h>
 
class protocol
{
    public:
        unsigned int version;
        unsigned int size;
        unsigned int headerSize;
        char *headerData;
        char *protocolData;
        char *headerData1;
        char *protocolData1;
        char *headerData2;
        char *protocolData2;
        char *headerData3;
        char *protocolData3;
 
    public:
        protocol(unsigned int version, unsigned int size, unsigned int headerSize, char *hData, char *pData): version(version),
                          size(size),
                          headerSize(headerSize)
        {
            std::cout<<"Inside constructor "<<strlen(hData)<<std::endl;
            headerData = new char[strlen(hData)];
            protocolData = new char[strlen(pData)];
            headerData1 = new char[strlen(hData)];
            protocolData1 = new char[strlen(pData)];
 
            headerData2 = new char[strlen(hData)];
            protocolData2 = new char[strlen(pData)];
 
            headerData3 = new char[strlen(hData)];
            protocolData3 = new char[strlen(pData)];
             
            std::copy(hData, (hData + strlen(hData)), headerData);
            std::copy(pData, (pData + strlen(pData)), protocolData);
            std::copy(hData, (hData + strlen(hData)), headerData1);
            std::copy(pData, (pData + strlen(pData)), protocolData1);
            std::copy(hData, (hData + strlen(hData)), headerData2);
            std::copy(pData, (pData + strlen(pData)), protocolData2);
            std::copy(hData, (hData + strlen(hData)), headerData3);
            std::copy(pData, (pData + strlen(pData)), protocolData3);
 
 
        }
 
        // copy constructor
        protocol(const protocol& copy): version(copy.version), size(copy.size), headerSize(copy.headerSize)
        {
            headerData = new char[strlen(copy.headerData)];
            protocolData = new char[strlen(copy.protocolData)];
 
            headerData1 = new char[strlen(copy.headerData1)];
            protocolData1 = new char[strlen(copy.protocolData1)];
 
            headerData2 = new char[strlen(copy.headerData2)];
            protocolData2 = new char[strlen(copy.protocolData2)];
 
            headerData3 = new char[strlen(copy.headerData3)];
            protocolData3 = new char[strlen(copy.protocolData3)];
 
            std::copy(copy.headerData2, (copy.headerData2 + strlen(copy.headerData2)), headerData2);
            std::copy(copy.protocolData2, (copy.protocolData2 + strlen(copy.protocolData2)), protocolData2);
            std::copy(copy.headerData1, (copy.headerData1 + strlen(copy.headerData1)), headerData1);
            std::copy(copy.protocolData1, (copy.protocolData1 + strlen(copy.protocolData1)), protocolData1);
            std::copy(copy.headerData, (copy.headerData + strlen(copy.headerData)), headerData);
            std::copy(copy.protocolData, (copy.protocolData + strlen(copy.protocolData)), protocolData);
            std::copy(copy.headerData3, (copy.headerData3 + strlen(copy.headerData3)), headerData3);
            std::copy(copy.protocolData3, (copy.protocolData3 + strlen(copy.protocolData3)), protocolData3);
 
            std::cout<<"Inside copy constructor "<<strlen(copy.headerData) <<std::endl;
        }
         
        //move constructor
        protocol(protocol&& copy): version(copy.version), size(copy.size), headerSize(copy.headerSize)
        {
            headerData = copy.headerData;
            protocolData = copy.protocolData;
            copy.protocolData = NULL;
            copy.headerData = NULL;
 
            headerData1 = copy.headerData1;
            protocolData1 = copy.protocolData1;
            copy.protocolData1 = NULL;
            copy.headerData1 = NULL;
 
            headerData2 = copy.headerData2;
            protocolData2 = copy.protocolData2;
            copy.protocolData2 = NULL;
            copy.headerData2 = NULL;
 
            headerData3 = copy.headerData3;
            protocolData3 = copy.protocolData3;
            copy.protocolData3 = NULL;
            copy.headerData3 = NULL;
 
            std::cout<<"Inside move constructor "<<strlen(headerData) <<std::endl;
        }
 
        ~protocol()
        {
            std::cout<<"Inside destructor "<<std::endl;
            delete[] headerData;
            delete[] protocolData;
            delete[] headerData1;
            delete[] protocolData1;
            delete[] headerData2;
            delete[] protocolData2;
            delete[] headerData3;
            delete[] protocolData3;
        }
};
 
int main()
{
    char *hd = "headerdatais123i666666666666666666666666666666666";
    char *pd = "protocoldatais99999999999999999988776655667788997";
 
    protocol p(1,200,strlen(hd),hd,pd); //Normal constructor
 
    protocol p1(*(new protocol(1,200,strlen(hd),hd,pd))); //Copy constructor
     
    protocol p2(std::move(*(new protocol(1,200,strlen(hd),hd,pd)))); // Move Constructor
     
    return 0;
}
