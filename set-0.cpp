#include <string>
#include <set>
#include <iostream>
void test(std::set<std::string> &t , std::string &s)
{
	t.insert(s);
	std::string hi("hi");
	t.insert(hi);
}
int main(int argc, char *argv[])
{
	std::set<std::string> s;
	std::string hi("hi");
	test(s, hi);
    	for(std::set<std::string>::iterator it = s.begin(); it!= s.end(); it++)
        	std::cout << *it << " " << std::endl;
}
