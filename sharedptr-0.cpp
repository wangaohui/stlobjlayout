#include <memory>
struct Base
{
    Base() { }
    ~Base() { }
};
 
struct Derived: public Base
{
    Derived() {  }
    ~Derived() {  }
};
 
int main()
{
    std::shared_ptr<Base> p = std::make_shared<Derived>();
}
