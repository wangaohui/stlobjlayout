#include <iostream>
#include <memory>
using namespace std;

void useShared_ptr(int *p)
{
    cout<<*p<<endl;
}

void delePointer(int *p)
{
    delete p;
}

int main(int argc, char *argv[])
{
    shared_ptr<int> p1 = make_shared<int>(32);
    weak_ptr<int> p2(p1);
    useShared_ptr(p1.get());
    return 0;
}
