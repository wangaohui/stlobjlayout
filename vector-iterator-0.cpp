#include <iostream>
#include <vector>

std::vector<int> myvector = {1, 2, 3, 4};
int getCandidateList(void)
{
	int i;
	for (std::vector<int>::const_iterator j = myvector.begin(); j < myvector.end(); j++)
	{
	    i = *j;
	}
	return i;
}

int main ()
{
  int a = getCandidateList();
  std::cout << a << std::endl;
  return 0;
}
